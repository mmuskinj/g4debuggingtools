import sys
sys.path.append('./')

import ROOT
import os, sys
import operator

from copy import deepcopy

from python.G4Debugger import *
from python.utils import *
from python.config import *

def shortName(string):
  return string.replace("G4.","").replace("Geant4.","").replace("patch","").replace("atlas","").replace(".0",".")

def absTH2(h):
  for i in range(0,h.GetNbinsX()+1):
    for j in range(0,h.GetNbinsY()+1):
      h.SetBinContent(i,j,abs(h.GetBinContent(i,j)))
  return h

def diffTH2(h1,h2):
  h = h1.Clone(h1.GetName()+"diff")
  for i in range(0,h1.GetNbinsX()+1):
    for j in range(0,h2.GetNbinsY()+1):
      v1 = h1.GetBinContent(i,j)
      v2 = h2.GetBinContent(i,j)
      if v1+v2 == 0:
        continue
      if v2 > v1:
        h.SetBinContent(i,j,100*(v2-v1)/v2)
      else:
        h.SetBinContent(i,j,100*(v1-v2)/v1)        
  return h



ROOT.gROOT.SetBatch(True)

makeOnlyWebPage = False

do1Dplots = True
do2Dplots = False


def fill(target, h, name = ""):
  if h:
    if target == None:
      target = h.Clone(name)
    else:
      target.Add(h)  
  return target

if __name__ == '__main__':

  filePath = sys.argv[1]
  G4Version1 = sys.argv[2]
  G4Version2 = sys.argv[3]
  G4Version1Label = sys.argv[4]
  G4Version2Label = sys.argv[5]
  
  G4D1 = G4Debugger(filePath,G4Version1)
  G4D2 = G4Debugger(filePath,G4Version2)

  # sys.exit(0)

  # G4Version1 = shortName(G4Version1)
  # G4Version2 = shortName(G4Version2)
  G4Version1 = G4Version1Label
  G4Version2 = G4Version2Label

  print G4Version1," ",G4Version2

  groups = [
    # particles,
    # egamma,
    # hadrons,
    # electrons,
    neutrons,
  ]

  for group in groups:

    output = G4Version1.replace(".","_") + "_vs_" + G4Version2.replace(".","_")
    if group == particles:
      output += "/all"
    elif group == egamma:
      output += "/egamma"
    elif group == hadrons:
      output += "/hadrons"
    elif group == electrons:
      output += "/electrons"
    elif group == neutrons:
      output += "/neutrons"

    # for p in G4D1.totalYields:
    #   print p," ",G4D1.totalYields[p]
    #   integral += G4D1.totalYields[p]

    dict1 = G4D1.volumeSummary
    dict2 = G4D2.volumeSummary
    integral1 = sum([sum(dict1[cat].values()) for cat in dict1])
    integral2 = sum([sum(dict2[cat].values()) for cat in dict2])
    print sum([sum(dict1[cat].values()) for cat in dict1])
    print sum([sum(dict2[cat].values()) for cat in dict2])

    if not os.path.exists("%s/plots" % output):
      os.makedirs("%s/plots" % output)


    # if "FTFP_BERT" in G4Version2:
    #   G4Version2 = "FTFP_BERT"
    # elif "QGSP_BIC" in G4Version2:
    #   G4Version2 = "QGSP_BIC"

    if not makeOnlyWebPage:

      # if G4Version1[0:4] == G4Version2[0:4] and "QGSP_BIC" not in G4Version2:
      #   ratio1 = 0.1
      #   ratio2 = 0.1
      #   print "OK"
      # else:
      ratio1 = 1.0
      ratio2 = 0.2

      name = "vol" + "".join(group)
      plotSummaryRatio(G4D1.volumeSummary,G4D2.volumeSummary, xaxis="Volumes",
        v1=G4Version1, v2=G4Version2, ratio = ratio1, mergeOther=True, directory=("%s/plots" % output), name=name)

      name = "mat" + "".join(group)
      plotSummaryRatio(G4D1.materialSummary,G4D2.materialSummary, xaxis="Materials",
        v1=G4Version1, v2=G4Version2, ratio = ratio2, mergeOther=True, directory=("%s/plots" % output), name=name)

      name = "prc" + "".join(group)
      plotSummaryRatio(G4D1.processSummary,G4D2.processSummary, xaxis="Processes",
        v1=G4Version1, v2=G4Version2, ratio = ratio2, mergeOther=True, directory=("%s/plots" % output), name=name)

      yaxis = "|G1 - G2| / max(G1, G2) [%]"

      if do1Dplots:

        for hisType in labelAllATLAS:
          dict1 = {}
          dict2 = {}
          print "=== initial steps ==="
          for p in group:
            if hisType == "averageNumberOfStepsPerInitialE":
              h1 = G4D1.GetHistogramAllATLAS("numberOfStepsPerInitialE","AllATLAS",p,"nSteps")
              h2 = G4D2.GetHistogramAllATLAS("numberOfStepsPerInitialE","AllATLAS",p,"nSteps")
              hh1 = G4D1.GetHistogramAllATLAS("InitialE","AllATLAS",p,"nSteps")
              hh2 = G4D2.GetHistogramAllATLAS("InitialE","AllATLAS",p,"nSteps")
              if h1 and h2:
                print "particle: %s %.2e %.2e" % (p, hh1.Integral(0,hh1.GetNbinsX()+1), hh2.Integral(0,hh2.GetNbinsX()+1))
                h1 = h1.Clone(h1.GetName().replace("number","averageNumber"))
                h2 = h2.Clone(h2.GetName().replace("number","averageNumber"))
                h1.Divide(hh1)
                h2.Divide(hh2)
            elif hisType == "CumulativeInitialE":
              h1 = G4D1.GetHistogramAllATLAS("InitialE","AllATLAS",p,"nSteps")
              h2 = G4D2.GetHistogramAllATLAS("InitialE","AllATLAS",p,"nSteps")
              if h1 and h2:
                h1 = h1.Clone(h1.GetName().replace("number","cumulativeNumber")).GetCumulative()
                h2 = h2.Clone(h2.GetName().replace("number","cumulativeNumber")).GetCumulative()
                h1.Scale(1./h1.GetBinContent(h1.GetNbinsX()))
                h2.Scale(1./h2.GetBinContent(h2.GetNbinsX()))
            elif hisType == "CumulativeNumberOfStepsPerInitialE":
              h1 = G4D1.GetHistogramAllATLAS("numberOfStepsPerInitialE","AllATLAS",p,"nSteps")
              h2 = G4D2.GetHistogramAllATLAS("numberOfStepsPerInitialE","AllATLAS",p,"nSteps")
              if h1 and h2:
                h1 = h1.Clone(h1.GetName().replace("number","cumulativeNumber")).GetCumulative()
                h2 = h2.Clone(h2.GetName().replace("number","cumulativeNumber")).GetCumulative()
                h1.Scale(1./h1.GetBinContent(h1.GetNbinsX()))
                h2.Scale(1./h2.GetBinContent(h2.GetNbinsX()))
            else:
              h1 = G4D1.GetHistogramAllATLAS(hisType,"AllATLAS",p,"nSteps")
              h2 = G4D2.GetHistogramAllATLAS(hisType,"AllATLAS",p,"nSteps")
            if h1:
              dict1[p] = h1.Clone()
            if h2:
              dict2[p] = h2.Clone()
          plotHistogramRatio(dict1,dict2,G4Version1,G4Version2,"All ATLAS",integral1,hisType,directory="%s/plots" % output)

        for hisType in label:
          dict1 = {}
          dict2 = {}
          for v in G4D1.volumeSummary:
            for p in group:
              h1 = G4D1.GetHistogram(hisType,v,p,"volumes")
              h2 = G4D2.GetHistogram(hisType,v,p,"volumes")
              if h1:
                if not p in dict1.keys():
                  dict1[p] = h1.Clone()
                else:
                  dict1[p].Add(h1)
              if h2:
                if not p in dict2.keys():
                  dict2[p] = h2.Clone()
                else:
                  dict2[p].Add(h2)
          plotHistogramRatio(dict1,dict2,G4Version1,G4Version2,"All ATLAS",integral1,hisType,directory="%s/plots" % output)

        for v in G4D1.volumeSummary:
          directory = "%s/plots/Volumes/%s" % (output,v)
          if not os.path.exists(directory):
              os.makedirs(directory)
          for hisType in label:
            lab = "%s" % v
            dict1 = {}
            dict2 = {}
            for p in group:
              h1 = G4D1.GetHistogram(hisType,v,p,"volumes")
              h2 = G4D2.GetHistogram(hisType,v,p,"volumes")
              if h1:
                dict1[p] = h1
              if h2:
                dict2[p] = h2
            plotHistogramRatio(dict1,dict2,G4Version1,G4Version2,lab,integral1,hisType,directory=directory)

        for m in G4D1.materialSummary:
          directory = "%s/plots/Materials/%s" % (output,m)
          if not os.path.exists(directory):
              os.makedirs(directory)
          for hisType in label:
            lab = "%s" % m
            dict1 = {}
            dict2 = {}
            for p in group:
              h1 = G4D1.GetHistogram(hisType,m,p,"materials")
              h2 = G4D2.GetHistogram(hisType,m,p,"materials")
              if h1:
                dict1[p] = h1
              if h2:
                dict2[p] = h2
            plotHistogramRatio(dict1,dict2,G4Version1,G4Version2,lab,integral1,hisType,directory=directory)

        for r in G4D1.processSummary:
          directory = "%s/plots/Processes/%s" % (output,r)
          if not os.path.exists(directory):
              os.makedirs(directory)
          for hisType in label:
            lab = "%s" % r
            dict1 = {}
            dict2 = {}
            for p in group:
              h1 = G4D1.GetHistogram(hisType,r,p,"processes")
              h2 = G4D2.GetHistogram(hisType,r,p,"processes")
              if h1:
                dict1[p] = h1
              if h2:
                dict2[p] = h2
            plotHistogramRatio(dict1,dict2,G4Version1,G4Version2,lab,integral1,hisType,directory=directory)

      if do2Dplots:

        h2Dtotal1 = None
        h2Dtotal2 = None
        for v in G4D1.volumeSummary:
          directory = "%s/plots/Volumes/%s" % (output,v)
          if not os.path.exists(directory):
              os.makedirs(directory)
          h1 = None
          h2 = None
          for p in group:
            htemp1 = G4D1.GetHistogram("2DMaps",v,p,"volumes")
            htemp2 = G4D2.GetHistogram("2DMaps",v,p,"volumes")
            h1 = fill(h1,htemp1,v+p+"1")
            h2 = fill(h2,htemp2,v+p+"2")
            h2Dtotal1 = fill(h2Dtotal1,htemp1,"AllATLAS1")
            h2Dtotal2 = fill(h2Dtotal2,htemp2,"AllATLAS2")
          plot2D(h1,G4Version1,v,integral1,"",directory=directory)
          plot2D(h2,G4Version2,v,integral2,"",directory=directory)
          if h1 and h2:
            h1diff = diffTH2(h1,h2)
            plot2D(h1diff,G4Version1+" vs G4."+G4Version2,v,0,yaxis,directory=directory)
        plot2D(h2Dtotal1,G4Version1,"All ATLAS",integral1,"",directory="%s/plots" % output)
        plot2D(h2Dtotal2,G4Version2,"All ATLAS",integral2,"",directory="%s/plots" % output)
        if h2Dtotal1 and h2Dtotal2:
          totalDiff = diffTH2(h2Dtotal1,h2Dtotal2)
          plot2D(totalDiff,G4Version1+" vs G4."+G4Version2,"All ATLAS",0,yaxis,directory="%s/plots" % output)

        for m in G4D1.materialSummary:
          directory = "%s/plots/Materials/%s" % (output,m)
          if not os.path.exists(directory):
              os.makedirs(directory)
          h1 = None
          h2 = None
          for p in group:
            htemp1 = G4D1.GetHistogram("2DMaps",m,p,"materials")
            htemp2 = G4D2.GetHistogram("2DMaps",m,p,"materials")
            h1 = fill(h1,htemp1,m+p+"1")
            h2 = fill(h2,htemp2,m+p+"2")
          plot2D(h1,G4Version1,m,integral1,"",directory=directory)
          plot2D(h2,G4Version2,m,integral2,"",directory=directory)
          if h1 and h2:
            h1diff = diffTH2(h1,h2)
            plot2D(h1diff,G4Version1+" vs G4."+G4Version2,m,0,yaxis,directory=directory)

        for r in G4D1.processSummary:
          directory = "%s/plots/Processes/%s" % (output,r)
          if not os.path.exists(directory):
              os.makedirs(directory)
          h1 = None
          h2 = None
          for p in group:
            htemp1 = G4D1.GetHistogram("2DMaps",r,p,"processes")
            htemp2 = G4D2.GetHistogram("2DMaps",r,p,"processes")
            h1 = fill(h1,htemp1,r+p+"1")
            h2 = fill(h2,htemp2,r+p+"2")
          if h1:
            plot2D(h1,G4Version1,r,integral1,"",directory=directory)
          if h2:
            plot2D(h2,G4Version2,r,integral2,"",directory=directory)
          if h1 and h2:
            h1diff = diffTH2(h1,h2)
            plot2D(h1diff,G4Version1+" vs G4."+G4Version2,r,0,yaxis,directory=directory)



    sortedMats = sorted(G4D1.materialSummary.iteritems(), key=lambda (k,v): sum(v.values()), reverse=True)
    sortedVols = sorted(G4D1.volumeSummary.iteritems(), key=lambda (k,v): sum(v.values()), reverse=True)
    sortedPrcs = sorted(G4D1.processSummary.iteritems(), key=lambda (k,v): sum(v.values()), reverse=True)

    title = "Geant4." + G4Version1 + " vs Geant4." + G4Version2 

    print G4Version1," ",G4Version2

    makeWebpage(G4Version1, G4Version2,
                materials=[x[0] for x in sortedMats], volumes=[x[0] for x in sortedVols], processes=[x[0] for x in sortedPrcs],
                title = title, directory="%s/" % output)
