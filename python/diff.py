import ROOT

def diffTH2(h1,h2):
  h = h1.Clone(h1.GetName()+"diff")
  for i in range(0,h1.GetNbinsX()+1):
    for j in range(0,h2.GetNbinsY()+1):
      v1 = h1.GetBinContent(i,j)
      v2 = h2.GetBinContent(i,j)
      if v1+v2 == 0:
        continue
      if v2 > v1:
        val = 100*(v2-v1)/v2
        if val < 99:
          h.SetBinContent(i,j,val)
      else:
        val = 100*(v1-v2)/v1
        if val < 99:
          h.SetBinContent(i,j,val)        
  return h

f1 = ROOT.TFile("dip.root","READ")
f2 = ROOT.TFile("test.root","READ")

h1 = f1.Get("Transportationother2").Clone("h1")
h2 = f2.Get("Transportationother2").Clone("h2")

h1.Scale(1./h1.GetSum())
h2.Scale(1./h2.GetSum())

h = diffTH2(h1,h2)

canv = ROOT.TCanvas("c","c",800,600)
canv.SetRightMargin(0.20)
canv.SetTopMargin(0.07)
# canv.SetLogz()
h.Draw("COLZ")
h.GetXaxis().SetTitle("z [mm]")
h.GetYaxis().SetTitle("r [mm]")
h.GetZaxis().SetTitleOffset(1.2)
h.GetXaxis().SetNdivisions(505)


canv.Print("diff.png")
