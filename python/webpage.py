import datetime
import os

def buttonType(currentDir, targetDir):
  if currentDir in targetDir:
    return "dropbtn4"
  else:
    return "dropbtn3"

class webpageDefs:

  htaccess = open('html/.htaccess', 'r').read()

  css = open('html/style.css', 'r').read()


  def makeNavBar(self, materials, volumes, processes, prefix = "", folder = "", cat = "", directory = ""):
    navbar = ""

    if len(materials) > 0:
      navbar += """<a href=\"%s\"><button class="dropbtn">All ATLAS</button></a>
  """ % prefix

    if len(materials) > 0:
      navbar += """<div class="dropdown">
  <button class="dropbtn2">Materials</button>
  <div class="dropdown-content">
"""
      for mat in materials:
        navbar += "<a href=\"%sMaterials/%s/\">%s</a>\n" % (prefix,mat,mat)
      navbar += """</div>
</div>"""

    if len(volumes) > 0:
      navbar += """<div class="dropdown">
  <button class="dropbtn2">Volumes</button>
  <div class="dropdown-content">
"""
      for vol in volumes:
        navbar += "<a href=\"%sVolumes/%s/\">%s</a>\n" % (prefix,vol,vol)
      navbar += """</div>
</div>"""

    if len(processes) > 0:
      navbar += """<div class="dropdown">
  <button class="dropbtn2">Processes</button>
  <div class="dropdown-content">
"""
      for prc in processes:
        navbar += "<a href=\"%sProcesses/%s/\">%s</a>\n" % (prefix,prc,prc)
      navbar += """</div>
</div>"""

    currentDir = ""
    if folder!="" and cat !="":
      currentDir = folder+"/"+cat
    navbar += """<a href=\"%s../all/%s\"><button class="%s">all</button></a>
<a href=\"%s../egamma/%s\"><button class="%s">e/gamma</button></a>
<a href=\"%s../hadrons/%s\"><button class="%s">hadrons</button></a>""" % (prefix, currentDir, buttonType("all",directory), 
                                                                          prefix, currentDir, buttonType("egamma",directory),
                                                                          prefix, currentDir, buttonType("hadrons",directory))

    return navbar

  def makeMainPage(self, G4Version1, G4Version2, title, materials, volumes, processes, summary, nstep, directory):
    body = """<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">
<html>
<head>
  <title>%s</title>
  <link rel="stylesheet" href="style.css">
</head>

<body>
""" % title
    body += self.makeNavBar(materials, volumes, processes, "", "", "", directory)
    body += """
<!-- Main content -->
<h1>%s</h1>

""" % title
    body += """
<div class="grid">"""

    if summary:
      body += """  <div><a href="plots/Materials.pdf"><img src="plots/Materials.png"></a></div>
  <div><a href="plots/Volumes.pdf"><img src="plots/Volumes.png"></a></div>
  <div><a href="plots/Processes.pdf"><img src="plots/Processes.png"></a></div>
  <div></div>
  """

    if nstep:

      body +=  """<div><a href="plots/numberOfSteps.pdf"><img src="plots/numberOfSteps.png"></a></div>
  <div><a href="plots/InitialE.pdf"><img src="plots/InitialE.png"></a></div>
  <div><a href="plots/averageNumberOfStepsPerInitialE.pdf"><img src="plots/averageNumberOfStepsPerInitialE.png"></a></div>
  """


    body += """<div><a href="plots/stepLength.pdf"><img src="plots/stepLength.png"></a></div>
  <div><a href="plots/stepKineticEnergy.pdf"><img src="plots/stepKineticEnergy.png"></a></div>
  <div><a href="plots/stepEnergyDeposit.pdf"><img src="plots/stepEnergyDeposit.png"></a></div>
  <div><a href="plots/stepSecondaryKinetic.pdf"><img src="plots/stepSecondaryKinetic.png"></a></div>
  <div><a href="plots/stepEnergyNonIonDeposit.pdf"><img src="plots/stepEnergyNonIonDeposit.png"></a></div>
  <div><a href="plots/stepPseudorapidity.pdf"><img src="plots/stepPseudorapidity.png"></a></div>
  <div><a href="plots/2DMapGeant4.%s.pdf"><img src="plots/2DMapGeant4.%s.png"></a></div>
  <div><a href="plots/2DMapGeant4.%s.pdf"><img src="plots/2DMapGeant4.%s.png"></a></div>
  <div><a href="plots/2DMapDiff.pdf"><img src="plots/2DMapDiff.png"></a></div>
</div>

<address>Made: %s<br></address>

</body>
</html>""" % (G4Version1, G4Version1, G4Version2, G4Version2, datetime.datetime.now().isoformat())
    return body

  def makeSubPage(self, G4Version1, G4Version2, title, cat, folder, materials, volumes, processes, directory):
    base = (folder,cat,folder,cat)
    strings = base
    for i in range(5):
      strings += (base)
    body = """<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">
<html>
<head>
  <title>%s</title>
  <link rel="stylesheet" href="../../style.css">
</head>

<body>
""" % title
    body += self.makeNavBar(materials, volumes, processes, "../../", folder, cat, directory)
    body += """
<!-- Main content -->
<h1>%s</h1>

""" % (folder+": "+cat)
    body += """
<div class="grid">
  <div><a href="../../plots/%s/%s/stepLength.pdf"><img src="../../plots/%s/%s/stepLength.png" alt="step length not available"></a></div>
  <div><a href="../../plots/%s/%s/stepKineticEnergy.pdf"><img src="../../plots/%s/%s/stepKineticEnergy.png" alt="step kinetic energy not available"></a></div>
  <div><a href="../../plots/%s/%s/stepEnergyDeposit.pdf"><img src="../../plots/%s/%s/stepEnergyDeposit.png" alt="step energy deposit not available"></a></div>
  <div><a href="../../plots/%s/%s/stepSecondaryKinetic.pdf"><img src="../../plots/%s/%s/stepSecondaryKinetic.png" alt="secondary kinetic energy length not available"></a></div>
  <div><a href="../../plots/%s/%s/stepEnergyNonIonDeposit.pdf"><img src="../../plots/%s/%s/stepEnergyNonIonDeposit.png" alt="step non-ionizing deposit not available"></a></div>
  <div><a href="../../plots/%s/%s/stepPseudorapidity.pdf"><img src="../../plots/%s/%s/stepPseudorapidity.png" alt="step pseudorapidity not available"></a></div>
""" % strings
    
    subDir = folder+"/"+cat+"/"

    body += """
<div><a href="../../plots/%s2DMapGeant4.%s.pdf"><img src="../../plots/%s2DMapGeant4.%s.png"></a></div>
<div><a href="../../plots/%s2DMapGeant4.%s.pdf"><img src="../../plots/%s2DMapGeant4.%s.png"></a></div>
<div><a href="../../plots/%s2DMapDiff.pdf"><img src="../../plots/%s2DMapDiff.png"></a></div>
</div>

<address>Made: %s<br></address>

</body>
</html>""" % (subDir, G4Version1, subDir, G4Version1, subDir, G4Version2, subDir, G4Version2, subDir, subDir, datetime.datetime.now().isoformat())
    return body

  def makeFirstPage(self, G4Version1, G4Version2, title, directory):
    body = """<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">
<html>
<meta http-equiv="Refresh" content="0.1; all/">
<head>
  <title>%s</title>
  <link rel="stylesheet" href="style.css">
</head>
""" % title

    body += self.makeNavBar([], [], [], directory)

    body += """<body>
<!-- Main content -->
<h1>%s</h1>
""" % title

    body += """<address>Made: %s<br></address>

</body>
</html>""" % datetime.datetime.now().isoformat()
    return body
  

webpage = webpageDefs()